#include <cmath>
#define PI 3.14

class Circle
{
	private:
		double radius, ference, area;
	public:
		Circle(double _radius = 1)
		{
			radius = _radius;
			ference = 2 * PI * radius;
			area = PI * radius * radius;
		}
		
		void set_radius(double _radius) 
		{ 
			radius = _radius;
			ference = 2 * PI * radius;
                        area = PI * radius * radius; 
		}

		void set_ference(double _ference) 
		{ 
			ference = _ference;
			radius = ference / (2 * PI);
			area = PI * radius * radius; 
		}

		void set_area(double _area) 
		{ 
			area = _area;
			radius = sqrt(area / PI);
			ference = 2 * PI * radius; 
		}

		double get_radius() { return radius; }

		double get_ference() { return ference; }

		double get_area() { return area; }	
};
