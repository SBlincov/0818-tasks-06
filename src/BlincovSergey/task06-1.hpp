//
//  task06-1.hpp
//  task06-1
//
//  Created by Blincov Sergey on 28.06.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

class Circle{
private:
	double area;
	double lengthOfCircle;
	double radius;
public:
	//Make a constructor that takes the radius value
	Circle (double price): radius(price){
			area = M_PI*radius*radius;
			lengthOfCircle = 2*M_PI*radius;
		};

	~Circle()
	{};

	double Circle::area(){
		return (*this).area;
	};

	double Circle::lengthOfCircle(){
		return (*this).lengthOfCircle;
	};

	/*
		Make converters for lengthOfCircle to Radius(lengthToRadius) and Area to Radius (areaToRadius)
	*/

	void lengthToRadius(double length){
		double radius;
		radius = length/(2*M_PI);
		Circle newc(radius);
		(*this)=newc;
	}

	void areaToRadius(double area){
		double radius;
		radius=sqrt(area /(M_PI));
		Circle newc(radius);
		(*this)=newc;
	}
};
