//
//  task06-1.cpp
//  task06-1
//
//  Created by Blincov Sergey on 28.06.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//
#include "task06-1.hpp"

int main()
{
	const double _PRICEFORMETROFROAD=1000;
	const double _PRICEFORMETROFFENCE=2000;
	Circle Pool(3);
	Circle PoolWithRoad(4);
	double road , fence; 
	fence = PoolWithRoad.FERENCE()*_PRICEFORMETROFFENCE;
	road = (PoolWithRoad.AREA()-Pool.AREA())*_PRICEFORMETROFROAD;
	printf ("Money on road: %.4llf\n 
			 Money on fence: %.4llf\n",road,fence);
	return 0;
}